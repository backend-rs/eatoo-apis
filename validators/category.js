'use strict'

const response = require('../exchange/response')

exports.canCreate = (req, res, next) => {
    if(!req.body.parent.categoryType){
        response.notFound(res, 'categoryType_required')
    }
    if(req.body.parent.categoryType=='outlet'){
        if(!req.body.parent.id){
            response.notFound(res, 'outletId_required')
        }
        if(!req.body.name){
            response.notFound(res, 'name_required')
        }
    }
    if(req.body.parent.categoryType=='subCategory'){
        if(!req.body.parent.id){
            response.notFound(res, 'categoryId_required')
        }
        if(!req.body.name){
            response.notFound(res, 'name_required')
        }
    }
    if(req.body.parent.categoryType=='addOns'){
        if(!req.body.parent.id){
            response.notFound(res, 'itemId_required')
        }
        if(!req.body.name){
            response.notFound(res, 'name_required')
        }
        if(!req.body.isSingle){
            response.notFound(res, 'isSingle_required')
        }
    }
    
    
    return next()
}