module.exports = {
    parent: {
        categoryType: 'string',
        id: 'string',
    },
    name: 'string',
    isRecommanded: 'string',
    isSingle: 'string'
}